import requests
from django.conf import settings

def get_weather(lat, lng):
    url = 'http://api.weatherapi.com/v1/forecast.json?key={}&q={},{}&days={}'.format(settings.WEATHERAPI_KEY, lat, lng, 3)
    r = requests.get(url, headers={'Content-Type': 'application/json'})

    print(url)
    # print(r.json())

    response = r.json()

    location = response['location']

    current = {
        'date' : response['current']['last_updated'],
        'temp': response['current']['temp_c'],
        'wind' : response['current']["wind_kph"],
        'humidity' : response['current']["humidity"],
        'precipitation' : response['current']["precip_in"],
        'condition' : response['current']['condition']['text']
    }

    forecast = []
    for day in response['forecast']['forecastday']:
        this_day = {
            'date' : day['date'],
            'max_temp' : day['day']['maxtemp_c'],
            'min_temp' : day['day']['mintemp_c'],
            'wind' : day['day']['maxwind_kph'],             # In KMPH
            'humidity' : day['day']['avghumidity'],
            'precipitation' : day['day']["totalprecip_in"], # Rain in Inches
            'condition' : day['day']['condition']['text']
        }
        forecast.append(this_day)

    return location, current, forecast


