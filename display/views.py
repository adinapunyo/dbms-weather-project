from django.shortcuts import render
from django.http import JsonResponse, HttpResponse

# Create your views here.

from . import weather
import geocoder

def index(request):
    if request.user.is_authenticated:
        return render(request, 'home.html')
    else:
        return render(request, 'index.html')
    
def api(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            lng = request.POST.get('lng')
            lat = request.POST.get('lat')
            ip = request.POST.get('ip')

            if ip != "":
                request.user.ip_address = ip
            if lat != "":
                request.user.latitude = lat
            if lng != "":
                request.user.longitude = lng

            if lat == "" or lng == "":
                g = geocoder.ipinfo(ip)
                lat = g.latlng[0]
                lng = g.latlng[1]

            request.user.save()

            location, current, forecast = weather.get_weather(lat, lng)

            return JsonResponse({'location':location, 'current':current, 'forecast':forecast})
        else:
            return HttpResponse("Invalid request method!")
    else:
        return HttpResponse("User not authenticated!")