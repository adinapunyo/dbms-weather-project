from django.db import models

# Create your models here.

from django.contrib.auth.models import AbstractUser
from .manager import UserManager

class User(AbstractUser):
    # Primary Key field
    username = None
    email = models.EmailField(unique=True)

    # Location Data
    ip_address = models.CharField(max_length=30, null=True, blank=True)
    latitude = models.CharField(max_length=50, null=True, blank=True)
    longitude = models.CharField(max_length=50, null=True, blank=True)

    # Verification and Login fields
    isEmailVerified = models.BooleanField(default=False)
    last_login_time = models.DateTimeField(null=True, blank=True)
    last_logout_time = models.DateTimeField(null=True, blank=True)

    # Configuration fields
    objects = UserManager()
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []